/**
 * @file
 * @author Martin Stejskal
 * @brief Routines which allow to work with 3D vectors
 */
// ===============================| Includes |================================
#include "math_vector.h"

#include <assert.h>
#include <string.h>

#include "math_fast.h"
// ================================| Defines |================================
/**
 * @brief Maximum value which can be used when calculating vector length
 *
 * Vector have 3 compounds: x,y, z. And all need to fit into 64 bit buffer
 * -> 2^31 / 3 (calculating vector length: sqrt(x*x + y*y + z*z))
 */
#define VECT_LEN_INT32_MAX_VALUE (715827881)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
static uint16_t _reverse_bit_order_16(uint16_t u16_src);

// =========================| High level functions |==========================
te_math_vect_err mv_vect_32_scale_symetric(ts_mv_vector_32 *ps_res,
                                           ts_mv_vector_32 s_vect_src,
                                           const int32_t i32_source_max,
                                           const int32_t i32_target_max) {
  if (s_vect_src.i32_x > i32_source_max) {
    s_vect_src.i32_x = i32_source_max;
  }
  if (s_vect_src.i32_x < (-1 * i32_source_max)) {
    s_vect_src.i32_x = -1 * i32_source_max;
  }

  if (s_vect_src.i32_y > i32_source_max) {
    s_vect_src.i32_y = i32_source_max;
  }
  if (s_vect_src.i32_y < (-1 * i32_source_max)) {
    s_vect_src.i32_y = -1 * i32_source_max;
  }

  if (s_vect_src.i32_z > i32_source_max) {
    s_vect_src.i32_z = i32_source_max;
  }
  if (s_vect_src.i32_z < (-1 * i32_source_max)) {
    s_vect_src.i32_z = -1 * i32_source_max;
  }

  // Clean result structure
  memset(ps_res, 0, sizeof(ts_mv_vector_32));

  // It is required some margin to avoid overflow
  if ((i32_source_max > (INT32_MAX / 1000)) ||
      (s_vect_src.i32_x > (INT32_MAX / 1000)) ||
      (s_vect_src.i32_x < (INT32_MIN / 1000)) ||

      (s_vect_src.i32_y > (INT32_MAX / 1000)) ||
      (s_vect_src.i32_y < (INT32_MIN / 1000)) ||

      (s_vect_src.i32_z > (INT32_MAX / 1000)) ||
      (s_vect_src.i32_z < (INT32_MIN / 1000))) {
    return MATH_VECT_INVALID_ARG;
  }

  // Division ratio is: source/target. To get more precise results multiply
  // it by 1000
  int32_t i32_div_ratio = (i32_source_max * 1000) / i32_target_max;

  ps_res->i32_x = (s_vect_src.i32_x * 1000) / i32_div_ratio;
  ps_res->i32_y = (s_vect_src.i32_y * 1000) / i32_div_ratio;
  ps_res->i32_z = (s_vect_src.i32_z * 1000) / i32_div_ratio;

  return MATH_VECT_OK;
}

void mv_vect_16_cut_off(ts_mv_vector_16 *ps_vect, uint16_t u16_max_value) {
  // Check pointers
  assert(ps_vect);

  if (ps_vect->i16_x > u16_max_value) {
    ps_vect->i16_x = u16_max_value;
  }

  if (ps_vect->i16_y > u16_max_value) {
    ps_vect->i16_y = u16_max_value;
  }

  if (ps_vect->i16_z > u16_max_value) {
    ps_vect->i16_z = u16_max_value;
  }
}

void mv_vect_16_swap_bits(ts_mv_vector_16 *ps_vect) {
  ps_vect->i16_x = _reverse_bit_order_16(ps_vect->i16_x);
  ps_vect->i16_y = _reverse_bit_order_16(ps_vect->i16_y);
  ps_vect->i16_z = _reverse_bit_order_16(ps_vect->i16_z);
}
// ========================| Middle level functions |=========================
te_math_vect_err mv_vect_32_length(ts_mv_vector_32 *ps_vect,
                                   int32_t *pi32_length) {
  if ((ps_vect->i32_x > VECT_LEN_INT32_MAX_VALUE) ||
      (ps_vect->i32_x < (-1 * VECT_LEN_INT32_MAX_VALUE)) ||
      (ps_vect->i32_y > VECT_LEN_INT32_MAX_VALUE) ||
      (ps_vect->i32_y < (-1 * VECT_LEN_INT32_MAX_VALUE)) ||
      (ps_vect->i32_z > VECT_LEN_INT32_MAX_VALUE) ||
      (ps_vect->i32_z < (-1 * VECT_LEN_INT32_MAX_VALUE))) {
    return MATH_VECT_INVALID_ARG;
  }

  // Calculating length: sqrt(x*x + y*y + z*z)
  // Do sum first
  int64_t i64_sum = (int64_t)ps_vect->i32_x * (int64_t)ps_vect->i32_x +
                    (int64_t)ps_vect->i32_y * (int64_t)ps_vect->i32_y +
                    (int64_t)ps_vect->i32_z * (int64_t)ps_vect->i32_z;

  *pi32_length = (int32_t)mf_sqrt_64((uint64_t)i64_sum);
  return MATH_VECT_OK;
}

te_math_vect_err mv_vect_16_length(ts_mv_vector_16 *ps_vect,
                                   int32_t *pi32_length) {
  // Input vector length will always fit into 32 bit value

  // Calculating length: sqrt(x*x + y*y + z*z)
  // Do sum first
  int64_t i64_sum = (int64_t)ps_vect->i16_x * (int64_t)ps_vect->i16_x +
                    (int64_t)ps_vect->i16_y * (int64_t)ps_vect->i16_y +
                    (int64_t)ps_vect->i16_z * (int64_t)ps_vect->i16_z;

  *pi32_length = (int32_t)mf_sqrt_64((uint64_t)i64_sum);
  return MATH_VECT_OK;
}

void mv_vect_16_abs(ts_mv_vector_16 *ps_vect) {
  assert(ps_vect);

  if (ps_vect->i16_x < 0) {
    ps_vect->i16_x = -1 * ps_vect->i16_x;
  }
  if (ps_vect->i16_y < 0) {
    ps_vect->i16_y = -1 * ps_vect->i16_y;
  }
  if (ps_vect->i16_z < 0) {
    ps_vect->i16_z = -1 * ps_vect->i16_z;
  }
}

void mv_vect_32_abs(ts_mv_vector_32 *ps_vect) {
  assert(ps_vect);

  if (ps_vect->i32_x < 0) {
    ps_vect->i32_x = -1 * ps_vect->i32_x;
  }
  if (ps_vect->i32_y < 0) {
    ps_vect->i32_y = -1 * ps_vect->i32_y;
  }
  if (ps_vect->i32_z < 0) {
    ps_vect->i32_z = -1 * ps_vect->i32_z;
  }
}

void mv_vect_16_get_minimum(ts_mv_vector_16 *ps_vect_vesult,
                            ts_mv_vector_16 *ps_vect_a,
                            ts_mv_vector_16 *ps_vect_b) {
  assert(ps_vect_vesult && ps_vect_a && ps_vect_b);

  if (ps_vect_a->i16_x < ps_vect_b->i16_x) {
    ps_vect_vesult->i16_x = ps_vect_a->i16_x;
  } else {
    ps_vect_vesult->i16_x = ps_vect_b->i16_x;
  }

  if (ps_vect_a->i16_y < ps_vect_b->i16_y) {
    ps_vect_vesult->i16_y = ps_vect_a->i16_y;
  } else {
    ps_vect_vesult->i16_y = ps_vect_b->i16_y;
  }

  if (ps_vect_a->i16_z < ps_vect_b->i16_z) {
    ps_vect_vesult->i16_z = ps_vect_a->i16_z;
  } else {
    ps_vect_vesult->i16_z = ps_vect_b->i16_z;
  }
}

void mv_vect_16_get_maximum(ts_mv_vector_16 *ps_vect_result,
                            ts_mv_vector_16 *ps_vect_a,
                            ts_mv_vector_16 *ps_vect_b) {
  assert(ps_vect_result && ps_vect_a && ps_vect_b);

  if (ps_vect_a->i16_x > ps_vect_b->i16_x) {
    ps_vect_result->i16_x = ps_vect_a->i16_x;
  } else {
    ps_vect_result->i16_x = ps_vect_b->i16_x;
  }

  if (ps_vect_a->i16_y > ps_vect_b->i16_y) {
    ps_vect_result->i16_y = ps_vect_a->i16_y;
  } else {
    ps_vect_result->i16_y = ps_vect_b->i16_y;
  }

  if (ps_vect_a->i16_z > ps_vect_b->i16_z) {
    ps_vect_result->i16_z = ps_vect_a->i16_z;
  } else {
    ps_vect_result->i16_z = ps_vect_b->i16_z;
  }
}
// ==========================| Low level functions |==========================
bool mv_vect_16_are_components_in_range(const ts_mv_vector_16 *ps_vect,
                                        const int16_t i16_min,
                                        const int16_t i16_max) {
  if ((ps_vect->i16_x > i16_max) || (ps_vect->i16_x < i16_min) ||
      (ps_vect->i16_y > i16_max) || (ps_vect->i16_y < i16_min) ||
      (ps_vect->i16_z > i16_max) || (ps_vect->i16_z < i16_min)) {
    // Not in range
    return false;
  } else {
    return true;
  }
}

void mv_vect_16_add(ts_mv_vector_16 *ps_vect_result, ts_mv_vector_16 *ps_vect_a,
                    ts_mv_vector_16 *ps_vect_b) {
  assert(ps_vect_result && ps_vect_a && ps_vect_b);

  ps_vect_result->i16_x = ps_vect_a->i16_x + ps_vect_b->i16_x;
  ps_vect_result->i16_y = ps_vect_a->i16_y + ps_vect_b->i16_y;
  ps_vect_result->i16_z = ps_vect_a->i16_z + ps_vect_b->i16_z;
}

void mv_vect_16_sub(ts_mv_vector_16 *ps_vect_result, ts_mv_vector_16 *ps_vect_a,
                    ts_mv_vector_16 *ps_vect_b) {
  assert(ps_vect_result && ps_vect_a && ps_vect_b);

  ps_vect_result->i16_x = ps_vect_a->i16_x - ps_vect_b->i16_x;
  ps_vect_result->i16_y = ps_vect_a->i16_y - ps_vect_b->i16_y;
  ps_vect_result->i16_z = ps_vect_a->i16_z - ps_vect_b->i16_z;
}

void mv_vect_16_mul(ts_mv_vector_16 *ps_vect_result, ts_mv_vector_16 *ps_vect_a,
                    ts_mv_vector_16 *ps_vect_b) {
  assert(ps_vect_result && ps_vect_a && ps_vect_b);

  ps_vect_result->i16_x = ps_vect_a->i16_x * ps_vect_b->i16_x;
  ps_vect_result->i16_y = ps_vect_a->i16_y * ps_vect_b->i16_y;
  ps_vect_result->i16_z = ps_vect_a->i16_z * ps_vect_b->i16_z;
}

void mv_vect_16_div(ts_mv_vector_16 *ps_vect_result,
                    ts_mv_vector_16 *ps_vect_numerator,
                    ts_mv_vector_16 *ps_vect_denominator) {
  // Check pointers
  assert(ps_vect_numerator);
  assert(ps_vect_denominator);
  assert(ps_vect_result);

  // We should not divide by zero. If so, raise error
  assert(ps_vect_denominator->i16_x);
  assert(ps_vect_denominator->i16_y);
  assert(ps_vect_denominator->i16_z);

  ps_vect_result->i16_x = ps_vect_numerator->i16_x / ps_vect_denominator->i16_x;
  ps_vect_result->i16_y = ps_vect_numerator->i16_y / ps_vect_denominator->i16_y;
  ps_vect_result->i16_z = ps_vect_numerator->i16_z / ps_vect_denominator->i16_z;
}
// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static uint16_t _reverse_bit_order_16(uint16_t u16_src) {
  u16_src = (u16_src & 0xFF00) >> 8 | (u16_src & 0x00FF) << 8;
  u16_src = (u16_src & 0xF0F0) >> 4 | (u16_src & 0x0F0F) << 4;
  u16_src = (u16_src & 0xCCCC) >> 2 | (u16_src & 0x3333) << 2;
  u16_src = (u16_src & 0xAAAA) >> 1 | (u16_src & 0x5555) << 1;

  return u16_src;
}
