/**
 * @file
 * @author Martin Stejskal
 * @brief Routines which allow to work with 3D vectors
 */
#ifndef __MATH_VECTOR_H__
#define __MATH_VECTOR_H__
// ===============================| Includes |================================
#include <stdbool.h>
#include <stdint.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================
/**
 * @brief Error codes used by math vector module
 *
 */
typedef enum {
  MATH_VECT_OK = 0,       /**< OK - no error detected */
  MATH_VECT_ERROR = 1,    /**< Generic error code */
  MATH_VECT_INVALID_ARG,  /**< Invalid input argument */
  MATH_VECT_EMPTY_POINTER /**< Empty pointer error */
} te_math_vect_err;
// =======================| Structures, enumerations |========================
/**
 * @brief Structure for 16 bit signed 3D vector
 */
typedef struct {
  int16_t i16_x;
  int16_t i16_y;
  int16_t i16_z;
} ts_mv_vector_16;

/**
 * @brief Structure for 32 bit signed 3D vector
 */
typedef struct {
  int32_t i32_x;
  int32_t i32_y;
  int32_t i32_z;
} ts_mv_vector_32;

/**
 * @brief Structure for 64 bit signed 3D vector
 */
typedef struct {
  int64_t i64_x;
  int64_t i64_y;
  int64_t i64_z;
} ts_mv_vector_64;

/**
 * @brief Structure for float 3D vector
 */
typedef struct {
  float f_x;
  float f_y;
  float f_z;
} ts_mv_vector_float;
// ===========================| Global variables |============================

// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Scale symmetrically given vector
 *
 * Ratio of i32_source_max and i32_target_max tells how vector should be scaled.
 * Note that full 32 bit range can not be used, to avoid overflow during
 * intermediate calculations. But most of data from sensors should fit into
 * range without problems.
 *
 * @param ps_res Pointer to result vector structure
 * @param s_vect_src Source vector
 * @param i32_source_max Together with @ref i32_target_max it define scale
 *                       ratio. Also it define maximum value in source vector.
 *                       If any value reach
 *                       this threshold, it will be cut off. This way it is
 *                       possible to make sure that input value does not cross
 *                       predefined level
 * @param i32_target_max Together with @ref i32_source_max it define scale
 *                       ratio. Also it define maximum value in result vector.
 *                       If any value reach
 *                       this threshold, it will be cut off. This way it is
 *                       possible to make sure that value does not cross
 *                       predefined level
 * @return MATH_VECT_OK if no error. MATH_VECT_INVALID_ARG if value is out of
 *         range
 */
te_math_vect_err mv_vect_32_scale_symetric(ts_mv_vector_32 *ps_res,
                                           ts_mv_vector_32 s_vect_src,
                                           const int32_t i32_source_max,
                                           const int32_t i32_target_max);

/**
 * @brief Cut off values in vector
 * @param ps_vect Pointer to vector data
 * @param u16_max_value Maximum value. When any part of vector exceed this
 * value, value will be changed to this maximum
 */
void mv_vect_16_cut_off(ts_mv_vector_16 *ps_vect, uint16_t u16_max_value);

/**
 * @brief Reverse bit order in every vector item
 * @param ps_vect Pointer to vector structure
 */
void mv_vect_16_swap_bits(ts_mv_vector_16 *ps_vect);
// ========================| Middle level functions |=========================
/**
 * @brief Calculate 32 bit long vector length
 * @param ps_vect Pointer to source vector
 * @param pi32_length Result will be written here
 * @return MATH_VECT_OK if no error. MATH_VECT_INVALID_ARG if value is out of
 *         range
 *
 * @note Length can not be negative. Signed integer is used for easier
 *       operations later on
 */
te_math_vect_err mv_vect_32_length(ts_mv_vector_32 *ps_vect,
                                   int32_t *pi32_length);

/**
 * @brief Calculate 16 bit long vector length
 * @param ps_vect Pointer to source vector
 * @param pi32_length Result will be written here
 * @return MATH_VECT_OK if no error. MATH_VECT_INVALID_ARG if value is out of
 *         range
 *
 * @note Length can not be negative. Signed integer is used for easier
 *       operations later on
 */
te_math_vect_err mv_vect_16_length(ts_mv_vector_16 *ps_vect,
                                   int32_t *pi32_length);

/**
 * @brief Get absolute value for every component in vector
 * @param ps_vect Pointer to vector structure
 */
void mv_vect_16_abs(ts_mv_vector_16 *ps_vect);

/**
 * @brief Get absolute value for every component in vector
 * @param ps_vect Pointer to vector structure
 */
void mv_vect_32_abs(ts_mv_vector_32 *ps_vect);

/**
 * @brief Get minimum value from both vectors
 * @param ps_vect_result Pointer to result structure
 * @param ps_vect_a Source vector A
 * @param ps_vect_b Source vector B
 */
void mv_vect_16_get_minimum(ts_mv_vector_16 *ps_vect_result,
                            ts_mv_vector_16 *ps_vect_a,
                            ts_mv_vector_16 *ps_vect_b);

/**
 * @brief Get maximum value from both vectors
 * @param ps_vect_result Pointer to result structure
 * @param ps_vect_a Source vector A
 * @param ps_vect_b Source vector B
 */
void mv_vect_16_get_maximum(ts_mv_vector_16 *ps_vect_result,
                            ts_mv_vector_16 *ps_vect_a,
                            ts_mv_vector_16 *ps_vect_b);

/**
 * @brief Check if every component fit into given range
 *
 * @param ps_vect Pointer to vector
 * @param i16_min Minimum value
 * @param i16_max Maximum value
 * @return True if X, Y and Z components are in given range. False otherwise
 */
bool mv_vect_16_are_components_in_range(const ts_mv_vector_16 *ps_vect,
                                        const int16_t i16_min,
                                        const int16_t i16_max);

/**
 * @brief Sum vector A and B (A+B)
 * @param ps_vect_result Pointer to result structure
 * @param ps_vect_a Source vector A
 * @param ps_vect_b Source vector B
 */
void mv_vect_16_add(ts_mv_vector_16 *ps_vect_result, ts_mv_vector_16 *ps_vect_a,
                    ts_mv_vector_16 *ps_vect_b);
/**
 * @brief Subtract vector B from vector A (A-B)
 * @param ps_vect_result Pointer to result structure
 * @param ps_vect_a Source vector A
 * @param ps_vect_b Source vector B
 */
void mv_vect_16_sub(ts_mv_vector_16 *ps_vect_result, ts_mv_vector_16 *ps_vect_a,
                    ts_mv_vector_16 *ps_vect_b);

/**
 * @brief Multiply vector A by vector B (A*B)
 * @param ps_vect_result Pointer to result structure
 * @param ps_vect_a Source vector A
 * @param ps_vect_b Source vector B
 */
void mv_vect_16_mul(ts_mv_vector_16 *ps_vect_result, ts_mv_vector_16 *ps_vect_a,
                    ts_mv_vector_16 *ps_vect_b);

/**
 * @brief Divide two 16 bit long vectors
 *
 * @param ps_vect_result Pointer to result structure
 * @param ps_vect_numerator Pointer to numerator vector ("top" part)
 * @param ps_vect_denominator Pointer to denominator vector ("bottom" part)
 *
 * @note When one of value of denominator is zero, assert will be raised.
 */
void mv_vect_16_div(ts_mv_vector_16 *ps_vect_result,
                    ts_mv_vector_16 *ps_vect_numerator,
                    ts_mv_vector_16 *ps_vect_denominator);
// ==========================| Low level functions |==========================

#endif  // __MATH_VECTOR_H__
